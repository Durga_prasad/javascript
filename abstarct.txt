" " is an online crowdfunding platform that enables anyone across India to raise funds for healthcare, education, sports, disaster relief and other personal causes, with great ease.
Crowdfunding can be a quick and easy way to meet unexpected, emergency needs. Today, anyone with a smartphone can participate in making a difference with great ease.
More and more people are now raising funds online to tackle emergencies more efficiently. Increasing digital access and the convenience of online payments are driving more and more Indians to take the digital route to organize greater support for urgent needs on time.
Crowdfunding is a collective effort by people who network and contribute collectively for a cause. 
This seems very similar to the traditional concept of charity or social cooperation but unlike funding here is done with an objective of getting repaid money or using for another cause . 
The modern day crowdfunding is associated with internet and the use of social media for fundraising.
 In India crowdfunding is still in its nascent stage even though the potential is incredibly high

